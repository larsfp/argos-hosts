Argos-hosts
===========

Plugin for Argos (https://github.com/p-e-w/argos). Might also work with Bitbar (https://getbitbar.com/) but needs some changes if editing menu should work.

![Example](img/hosts.png "Example")

Installation
============

Symlink script to your argos dir. I.e.

```bash 
$ ln -s ~/git/argos-hosts/hostsfile.py ~/.config/argos/hostsfile.1m.py
``` 

Copy and edit hostsfile.txt as you wish. It can be used to keep stuff in hosts that you don't want to be notified about, like local containers.

License
=======

Copyright dev@falkp.no. AGPL
