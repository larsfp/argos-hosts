#!/usr/bin/env python3
# Check active hosts file entries and warn

# GPL, dev@falkp.no

# TODO:
# dns servers?
# systemd-resolve --status
# look-up real IPs?

import os

def main():
    hostfile="/etc/hosts"
    configfile="hostsfile.txt"
    active_count=0
    content=''
    ignore=[
        '#',
        '127.0',
        'f',
        '::'
    ]

    configfile = os.path.dirname(os.path.realpath(__file__)) + os.path.sep + configfile

    try:
        with open(configfile) as f:
            for line in f:
                if 0 == len(line.strip()) or line.strip().startswith('#'): # Skip empty lines
                    continue
                ignore.append(line.strip())
    except IOError: # No config file
        pass

    with open(hostfile) as f:
        for line in f:
            if 0 == len(line.strip()): # Skip empty lines
                continue
            for i in ignore: # Skip standard stuff
                if line.startswith(i):
                    #print ("Skipping %s" % line)
                    break
            else:
                content += line

    active_count = len(content.splitlines())

    print("{}| size=10".format('📑%s' % (active_count if 0 < active_count else '')))
    print ("""---
%s entries in hosts file:

%s

---
Edit %s | iconName=gedit bash="%s" terminal=false
Edit excluded entries | iconName=gedit bash="%s" terminal=false
""" % (
    active_count, content, hostfile, 
    '/usr/bin/gedit admin://' + hostfile, 
    '/usr/bin/gedit ' + configfile
    ))

if __name__ == '__main__':
    main()
